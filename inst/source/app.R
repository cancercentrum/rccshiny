library(rccShiny)

load("./data/data.RData")

rccShinyApp(
  optionsList = optionsList,
  preconfig_object = preconfig_object,
  preconfig_object_list_sections_breaks = preconfig_object_list_sections_breaks,
  preconfig_object_list_sections_titles = preconfig_object_list_sections_titles,
  pageTitle = optionsList$pageTitle
)
